﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login

        sunbeamExamdbEntities dbEntities = new sunbeamExamdbEntities();
        // GET: Login
        public ActionResult SignIn()
        {

            return View();
        }

        [HttpPost]
        public ActionResult SignIn(EmployeeDetail loginObject, string ReturnUrl)
        {
            var Matchcount = (from emp in dbEntities.EmployeeDetails.ToList()
                              where emp.username == loginObject.username
                              &&
                              emp.password == loginObject.password
                              select emp).ToList().Count();
            if (Matchcount == 1)
            {
                FormsAuthentication.SetAuthCookie(loginObject.username, false);

                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);

                }
                else
                {
                    return Redirect("/home/Index");
                }

            }
            else
            {
                ViewBag.ErrorMessage = " username or password is not correct";
                return Redirect("/home/Index");

            }




        }
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/home/SignIn");
        }
    }
}