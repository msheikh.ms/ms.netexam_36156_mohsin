﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class LeaveTypeController : Controller
    {
        sunbeamExamdbEntities dbEntities = new sunbeamExamdbEntities();

        // GET: LeaveType
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {

            ViewBag.Message = "Please Update Record Here!";

            LeaveType empToBeEdited = (from emp in dbEntities.LeaveTypes.ToList()
                                   where emp.LeaveType_id == id
                                   select emp).First();

            return View(empToBeEdited);
        }

        [HttpPost]
        public ActionResult Edit(LeaveType leaveInserted)
        {
            var v = ViewBag.Message;
            LeaveType leaveToInsert = (from emp in dbEntities.LeaveTypes.ToList()
                                   where emp.LeaveType_id == leaveInserted.LeaveType_id
                                   select emp).First();

            leaveToInsert.LeaveTypeName = leaveInserted.LeaveTypeName;
            leaveToInsert.LeaveTypeDescription = leaveInserted.LeaveTypeDescription;
            leaveToInsert.NoOfLeavesPerYear = leaveInserted.NoOfLeavesPerYear;

            dbEntities.SaveChanges();

            return Redirect("/homee/Index");
        }




    }
}