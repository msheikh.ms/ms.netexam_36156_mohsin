﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class LeaveApplicationController : Controller
    {
        sunbeamExamdbEntities dbEntities = new sunbeamExamdbEntities();

        // GET: LeaveApplication
        public ActionResult Index()
        {
            var leaveapp = dbEntities.LeaveApplicationDatas.ToList();
            return View(leaveapp);
           
        }
    }
}