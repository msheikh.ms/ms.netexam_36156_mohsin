﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
namespace WebApplication2.Controllers
{
    [Authorize]
    public class homeController : Controller
    {
        sunbeamExamdbEntities dbEntities = new sunbeamExamdbEntities();
        // GET: home
        public ActionResult Index()
        {

            var allEmps = dbEntities.EmployeeDetails.ToList();
            return View(allEmps);
        }

        public ActionResult Delete(int id)
        {

            EmployeeDetail empToBeDeleted = (from emp in dbEntities.EmployeeDetails.ToList()
                                             where emp.Emp_id == id
                                             select emp).First();
            dbEntities.EmployeeDetails.Remove(empToBeDeleted);
            dbEntities.SaveChanges();
            return Redirect("/home/Index");

        }



        public ActionResult Edit(int Emp_id)
        {

            ViewBag.Message = "Please Update Record Here!";

            EmployeeDetail empToBeEdited = (from emp in dbEntities.EmployeeDetails.ToList()
                                   where emp.Emp_id == Emp_id
                                   select emp).First();

            return View(empToBeEdited);
        }



        [HttpPost]
        public ActionResult Edit(EmployeeDetail empUpdated)
        {
            var v = ViewBag.Message;
            EmployeeDetail empToBeEdited = (from emp in dbEntities.EmployeeDetails.ToList()
                                   where emp.Emp_id == empUpdated.Emp_id
                                   select emp).First();

            empToBeEdited.EmpName = empUpdated.EmpName;
            empToBeEdited.Address = empUpdated.Address;
            empToBeEdited.Designation = empUpdated.Designation;
            empToBeEdited.Dept_id = empUpdated.Dept_id;
            empToBeEdited.Country_id = empUpdated.Country_id;
            empToBeEdited.City_id = empUpdated.City_id;
            empToBeEdited.Contact_no = empUpdated.Contact_no;
            empToBeEdited.Email_id = empUpdated.Email_id;



            dbEntities.SaveChanges();

            return Redirect("/homee/Index");
        }




        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(EmployeeDetail empToBeAdded)
        {
            dbEntities.EmployeeDetails.Add(empToBeAdded);
            dbEntities.SaveChanges();

            return Redirect("/home/Index");
        }



        



    }
}